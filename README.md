**项目说明** 

- jr-boot是一个轻量级的，前后端分离的Java快速开发平台，能快速开发项目并交付
- 支持MySQL、Oracle、SQL Server、PostgreSQL等主流数据库

**具有如下特点** 

- 友好的代码结构及注释，便于阅读及二次开发
- 实现前后端分离，通过token进行数据交互，前端再也不用关注后端技术
- 灵活的权限控制，可控制到菜单、列表、按钮、表单，满足绝大部分的权限需求
- 配套前端框架使用Vue3.x，最新的技术，最好的性能、极大的提高了开发效率
- 引入power-job定时任务，可动态完成任务的添加、修改、删除、暂停、恢复及日志查看等功能
- 引入Hibernate Validator校验框架，轻松实现后端校验
- 引入云存储服务，已支持：七牛云、阿里云、腾讯云、华为云、minio等
- 引入knife4J文档支持，方便编写API接口文档
- 引入sms4j支持，支持多种短信平台
- 引入ureport报表支持
- 引入liteflow框架 开发规则引擎功能
- 引入magic-api 封装 实现在线开发接口
- 引入hutool包 作为工具类 
- 接入chatgpt，只需要配置中填写相应key 即可使用chatgpt赋能
- 使用mybatisplusjoin 增强mybatisplus 实现多表联查功能
- 使用satoken 作为权限框架 功能强大 好用
- 完善的代码生成器，可以自动生成各种各样的代码，包括：DTO、VO、Service、Controller、Mapper、JavaBean、Swagger等
- 支持一键发布docker镜像，方便快速部署
- 完善的xss防御 sql注入防御 黑白名单 接口限流。
- 使用camunda工作流引擎 为基础 实现符合中国的流程使用功能。
- 项目中除了个别业务逻辑需要，使用ORM完全实现零SQL编程。可以无需再编写XML文件，更好的适配所有的数据库.

**项目结构** 

```
jr-boot
├─common 公共模块
│  ├─annotation 常用注解
│  ├─aspect 系统日志
│  ├─exception 异常处理
│  ├─enums 后台枚举
│  ├─constant 后台常量
│  ├─handler 处理器
│  ├─model 常用数据模型
│  │   ├─base 实体类父类
│  │   ├─datasrouce 数据源有关模型封装
│  │   ├─generator 代码生成器有关模型封装
│  │   ├─result 返回值封装
│  │   └─tree 树结构模型封装
│  ├─page 分页工具类
│  ├─satoken 权限框架注解鉴权实现类
│  ├─tree 树结构接口  --用于限定数据结构
│  ├─utils 各种工具类
│  └─xss XSS过滤
│ 
├─config 配置信息
│ 
├─modules 功能模块
│  ├─bi 数据大屏模块
│  ├─generator 代码生成器模块
│  ├─oss 文件服务模块
│  ├─organization 组织架构模块
│  ├─form 自定义表单模块
│  ├─report 报表模块
│  └─system 系统管理模块
│ 
├─JrCodeApplication 项目启动类
│  
├──resources 
│  ├─mapper SQL对应的XML文件 
│  │─template freemark模板文件
│  │─banner 启动banner图 
│  └─logback 日志配置文件

```

<br/>

**技术选型：** 

- 项目 [jdk](https://www.oracle.com/java/technologies/downloads/)  采用  11 版本
- 开发框架 采用 [springboot ](https://spring.io/projects/spring-boot)版本 2.7.5 +
- ORM框架 采用 [mybatis-plus](https://baomidou.com/) 版本 3.5.2+
- ORM动态数据源 采用 [dynamic-datasource](https://www.kancloud.cn/tracy5546/dynamic-datasource/2264611) 版本 3.5.1
- ORM多表联查工具类 采用 [mybatis-plus-join](https://https://gitee.com/best_handsome/mybatis-plus-join/wikis/%E5%AE%89%E8%A3%85) 版本 1.2.2+
- ORM动态数据源 采用 [mybatis-plus-dynamic-datasource](https://gitee.com/baomidou/dynamic-datasource-spring-boot-starter) 版本 3.5.1
- 数据库连接池框架采用 [druid](https://github.com/alibaba/druid) 版本 1.1.22+
- 接口文档框架采用 [knife4j](https://doc.xiaominfo.com/) 版本 2.0.7+
- json格式化工具 采用 [fastjson](https://github.com/alibaba/fastjson/wiki/Quick-Start-CN)等 版本 1.2.72+
- 代码模板框架 采用 [Freemark](http://freemarker.foofun.cn/) 版本 2.3.30+
- 权限框架采用 [sa-token](https://sa-token.dev33.cn/) 版本 1.29.0+
- 常用工具类库 采用 [hutool](https://www.hutool.cn/) 版本5.7.20+
- 注解验证 [hibernate-validator](https://hibernate.org/validator/) 版本 6.0.13.Final
- 分布式定时任务框架 采用 [power-job](http://www.powerjob.tech/) 版本 4.3.2
- 工作流引擎 采用 [camunda](https://camunda.com/) 版本 7.18.0
- 在线接口 采用 [magic-api](https://www.ssssssss.org/magic-api/) 版本 2.1.1
- 短信工具类库 采用 [sms4j](https://wind.kim/) 版本 2.1.0
- java动态类库 采用 [javassist](http://www.javassist.org/) 版本 3.29.2-GA
- 规则引擎 采用 [liteflow](https://liteflow.yomahub.com/) 版本 2.10.1
- excel工具类库 采用 [easyexcel](https://github.com/alibaba/easyexcel) 版本 3.1.4
- 云存储 适配 平台（[minio](http://www.minio.org.cn/),阿里云,腾讯云,华为云,七牛云）

**注意事项：**
- idea 必须安装lombok插件使用
- 开发或者部署 需要在application.yml 设置相应的 yml
- idea 直接使用 dockerfile 打包上传到镜像 需要配置docker  docker所在服务器也需要打开2375端口
- pom 里面docker-plugin 插件 用途在于 执行打包命令 会直接上传到docker  也需要打开2375端口
- 代码生成器 如果想要拥有审计字段（create_user_id 等字段）自动填充功能 表结构必须要符合框架的约定 比需要拥有这些字段（create_user_id | create_date | modyfy_user_id | modify_date | delete_mark | enabled_mark）
- 框架默认使用jdk11 您也可以自己修改为jdk8  原则上jdk版本 最低jdk8 上不封顶  。
- 如果哪些接口您需要记录日志，请自行在控制器 controller 的方法上 添加 XjrLog注解 参数填写接口描述即可
- 如果您需要放行某些接口 /src/main/java/com/xjrsoft/config/SaTokenConfig.java 文件 对应新增
- 非数据优先的代码生成器功能  用户所配置的表结构 表名  字段名 如果配置的为大小写混用 默认会改为 蛇形命名法 而且默认全小写 （oracle  dm 等数据库默认全大写的除外）
- 代码生成器-数据优先 模板  如果 子表的外键 关联 父表的非主键字段  级联删除会失效。 如果需要级联删除 请默认子表外键 关联 父表的主键
- 如果需要使用定时任务 请根据 power-job 官网文档部署服务端  再来使用。 定时任务新增或者修改数据 将不会自动填充 审计字段
- 前端想要内嵌其他url  需要把外链地址填写到组件地址栏
- 工作流模块中 审批人、传阅人 数量不能超过21人

界面图片：
![输入图片说明](%E8%A1%A8%E5%8D%95%E5%B8%83%E5%B1%80.png)
![输入图片说明](%E8%A1%A8%E5%8D%952.png)
![输入图片说明](%E8%A1%A8%E5%8D%95%E8%AE%BE%E8%AE%A11.png)
![输入图片说明](image.png)
微信群：
![输入图片说明](%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20240407164258.jpg)
QQ群
![输入图片说明](qq.jpg)
