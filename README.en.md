# jrcode

#### Description
低代码开发平台：jrcode旨在为企业开发业务系统提供一整套解决方案，具有高效率、低代码、功能丰富等特点。使用表单式构建应用，您不用考虑公共组件、代码冗余、数据字典、文件库、智能表单、工作流、微服务互相调用、全局跟踪定位bug、多主键crud、复杂sql查询等各种问题，这些问题的解决方案都作扩展功能内置。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
